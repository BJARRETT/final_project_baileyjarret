"""
A python module for calculating time series of the
North Pacific Index and it's weighted running average
"""
import numpy as np
import datetime as dt
import xarray as xr

def NorthPacific_Index(pressure, months=[1,2,3,4,5,6,7,8,9,10,11,12],run_avg=7,run_weights=[1/24,3/24,5/24,6/24,5/24,3/24,1/24]):
    """ Calculate NPI and weighted running average
    based on surface pressure data in xarray dataset.
    PARAMETERS
    ----------
    pressure : The xarray dataset of surface pressure.
    
    months : Months the NPI should be calculated for. Defaults to all months.
    
    run_avg: Period of the runnning average. Defaults to 7.
    
    RETURNS
    ----------
    npi : The NPI time series.
    
    sigma: The standard deviation used in calculating the NPI.
    
    smooth: The weighted running average time series.
    """
    #=========================================================
    # Manipulate datatset to isolate desired months
    # and lats and lons that corespond to the NPI.
    #=========================================================
    pres_np = pressure.isel(time=pressure.time.dt.month.isin(months),lat=slice(10,25),lon=slice(64,89))/100
    
    #=========================================================
    # Cosine weighting of the latitudes. Applying
    # these weights to the dataset and finding the
    # area weighted mean.
    #=========================================================
    coslat_np = np.cos(np.deg2rad(pres_np.lat))
    coslat_np.name = 'weights'
    
    pres_np_weighted = (pres_np.weighted(coslat_np))
    pres_npi = pres_np_weighted.mean(('lon','lat'))
    #=========================================================
    # Finding the standard deviation of the area
    # weighted mean, and using it to standardize
    # the data and create time series.
    #=========================================================
    sigma = np.std(pres_npi.pres)
    npi = (pres_npi - np.mean(pres_npi.pres))/(np.std(pres_npi.pres))
    
    #=========================================================
    # Setting up the weights for the running mean.
    # Using the weights to create the weighted runnning
    # mean time series.
    #=========================================================
    wght = xr.DataArray(run_weights, dims=['window'])
    smooth = npi['pres'].rolling(time=run_avg, center=True).construct('window').dot(wght)
    #=========================================================
    # Return both time series and standard deviation.
    #=========================================================
    return npi,sigma,smooth
