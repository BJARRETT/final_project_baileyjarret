from setuptools import setup

setup(name = "NorthPacificIndex",
    version = "1.0.1",
    author = "Bailey Jarrett",
    packages=['NorthPacific_Index'],
    install_requires=['numpy','xarray','datetime'])
