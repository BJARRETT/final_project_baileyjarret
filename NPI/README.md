This package calculates the North Pacific Index (NPI)
as cited in Trenberth & Hurrell (1994). The NPI is the
standardized area-weighted sea level pressure over the region 
30°N-65°N, 160°E-140°W. The function will take surface air
pressure data and output the a time series of NPI values,
the standard deviation of the data, and a time series of
weighted running average "smooth" of the data. 
