# Final_Project_BaileyJarret

Final Project for AOS 573

Final_Project.ipynb : Main jupyter notebook containing final project code, plots, and analysis.

netcdf_editor.ipynb : Secondary jupyter notebook where necessary data from larger netcdf files were extracted to smaller files.

uwnd folder: NCEP-DOE Reanalysis 2 Data - Daily Mean U Wind at 10hPa 60N

pres_mon: NCEP-DOE Reanalysis 2 Data - Monthly Mean surface pressure

pres: NCEP-DOE Reanalysis 2 Data - Daily Mean surface pressure

NPI: Package for NorthPacific_Index function/module
